import React from 'react';
import PropTypes from 'prop-types';
import style from './button.module.scss'
export const typeButton = ['default-p-s', 'default-p-m','default-p-l', 'default-s-s', 'default-s-m','default-s-l'];
//export default { title: "MDX/Button" component: Button };
const Button = ({ testId, textButton, typeButton, actionButton }) => (
  <button data-testid={testId} 
    className={ 
    `${style[`button--${typeButton}`]} 
    ${style[`button`]} 
    ` } 
    onClick={actionButton}>
   {textButton}
  </button>
);
Button.propTypes = {
  typeButton: PropTypes.oneOf(['default-p-s', 'default-p-m','default-p-l', 'default-s-s', 'default-s-m','default-s-l']),
  textButton: PropTypes.string,
  testId: PropTypes.string,
  actionButton: PropTypes.any
}

Button.defaultProps = {
  typeButton: 'default-p-m',
  textButton: 'Soy un Botón',
  testId: 'botonPrimario',

}
export default Button; 