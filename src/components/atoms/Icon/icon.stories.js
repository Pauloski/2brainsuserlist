import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text,  select } from "@storybook/addon-knobs";
import Icon, {typeIcon }  from './index';

export default {
  title: "Storybook Knobs",
  decorators: [withKnobs]
};


storiesOf('Atoms.Icon', module)
.addDecorator(withKnobs)
.add('Icon ', () => <Icon
  testId={ text('id', 'default')} 
  typeIcon={select('tipo de icono',typeIcon, typeIcon[0])} 
  />
  )
