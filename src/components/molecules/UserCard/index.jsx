import React from 'react';
import PropTypes from 'prop-types';
import Title from '../../atoms/Title';
import Icon from '../../atoms/Icon';
import Button from '../../atoms/Button'
import './userCard.scss';

//import style from './button.module.scss'
//export const typeButton = ['primaryBlue', 'primaryRed', 'primaryGreen'];
//export default { title: "MDX/Button" component: Button };
const UserCard = ({ id, index, email, nameTitle, nameFirst, nameLast, avatar, typeDisplay, action }) =>{
  return(
    <div className={ typeDisplay ? `user-card` : `user-card--list` } data-testid={id} >
 <div className={ typeDisplay ? `user-card__id` : `user-card--list__id` }>
   <span>id:</span>
   <span>{id}</span>
  
 </div>
<div className={ typeDisplay ? `user-card__avatar` : `user-card--list__avatar` }>
<img src={`${avatar}`} alt={`${nameFirst}  ${nameLast}`}/>
</div>
  <div className={ typeDisplay ? `user-card__name` : `user-card--list__name` }> 
  <span className={ typeDisplay ? `user-card__name-title` : `user-card--list__name-title` }>{nameTitle} </span>
   
    <Title testId={`userId--${id}`} 
      textComponent={`${nameFirst}  ${nameLast}`} 
      fontSize="large"
      weight="bold" 
      align={ typeDisplay ? `center` : `left` }
      />
  </div>
  <div className={ typeDisplay ? `user-card__email` : `user-card--list__email` }  > 
    <span className={ typeDisplay ? `user-card__email-icon` : `user-card--list__email-icon` }><Icon typeIcon="mail-1" /></span>
    <span>  <a href={`mailto:${email}`}>{email} </a> </span>
   
    </div>
    <div className={ typeDisplay ? `user-card__button` : `user-card--list__button` } > 
      <Button typeButton="default-p-s" textButton="Ver perfil" actionButton={action} />
    </div>
</div>
)
} 
  


UserCard.propTypes = {
  testId: PropTypes.string,
  data: PropTypes.any
}

UserCard.defaultProps = {
  testId: 'botonPrimario',
  data: PropTypes.any

}
export default UserCard; 

