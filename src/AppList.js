import React, { Component } from 'react';
import Title from './components/atoms/Title';
import Icon from './components/atoms/Icon';
import UserCard from './components/molecules/UserCard';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      person:[],
      personDisplay:false, 
      activeIndex: false,
      displayView: true,
    };
  }
  


  componentDidMount() {
    var users = 'https://randomuser.me/api/?results=32';
    fetch(users)
    .then(response => response.json())
    .then((el) => {
    this.setState({ data: el.results });
    this.setState({ activeIndex: true })
    console.log(el.results[5])
    })
  
  }
 

  list = () => {
    this.setState({
      displayView: false,
    });
  }
  mosiac = () => {
    this.setState({
      displayView: true,
    });
  }
  

 

closeDetails =(index)=> {
  this.setState({
    personDisplay:false
  });
  document.querySelector(`.more-info-${index}`).classList.add("more-info--close");
  document.querySelector(`.more-info-${index}`).classList.remove("more-info--show");
  document.querySelector(`.bkg-modal`).classList.add("bkg-modal--close");
  document.querySelector(`.bkg-modal`).classList.remove("bkg-modal--show");
}



moreDetails =(index)=> {
  this.setState({
    personDisplay:true
  });
  this.setState({ person:this.state.data[index] });
  document.querySelector(`.more-info-${index}`).classList.add("more-info--show");
  document.querySelector(`.more-info-${index}`).classList.remove("more-info--close");
  document.querySelector(`.bkg-modal`).classList.add("bkg-modal--show");
  document.querySelector(`.bkg-modal`).classList.remove("bkg-modal--close");
}




  render() {
    const { activeIndex, data, displayView} = this.state

    if(!activeIndex) {
      return (
        <div className="loading">
         <img src={ require('./assets/image/loading.gif')}alt="loading app"/>
        </div>
        )
    } else {
      return (
        <>
        <div className="header"  >
        <div className="header__title">
        <Title textComponent="#We Love C<>od3" fontSize={"caption"} align ="left" weight="bold"/>
        </div>  
        
        </div>  
        
        <div className="wrapper-app" >
          <div className="icon-wrapper">
          <Title textComponent="Our Team Members" fontSize="caption" align ="left" weight="bold"/>
        
          <span className={ displayView ? `icon-view  icon-view--last icon-view--active ` : `icon-view  icon-view--last ` } onClick={ this.mosiac }><Icon typeIcon="th"/></span>
            <span className={ displayView ? `icon-view  ` : ` icon-view--active icon-view` } onClick={ this.list }><Icon typeIcon="th-list"/></span>
         
         </div>
          { data.map((item, index) =>
           <>
           <div key={index}>
            < UserCard  id={item.id.value} 
              index={index} 
              email={item.email} 
              nameTitle ={item.name.title} 
              nameFirst={item.name.first} 
              nameLast={item.name.last} 
              avatar={item.picture.large} 
              typeDisplay= {displayView}
              action={this.moreDetails.bind(this, index)}
              />

<div className={ `more-info more-info-${index} more-info--close ` }>
        <div className="more-info__wrapper">
        <span  className="more-info__cancel" onClick={ this.closeDetails.bind(this, index)}><Icon typeIcon="cancel" /></span>
        <div className="more-info__image">
          <img src={`${item.picture.large}`} alt={`${item.name.first}  ${item.name.last}`}/>
        </div>
        <div className="more-info__info">
          <span > <strong>Id: </strong>{item.id.value}</span >
          <span ><strong>Name: </strong>{item.name.title} {item.name.first} {item.name.last} </span >
          <span > <strong>Direction: </strong>{item.location.street.name} {item.location.street.number}, {item.location.city} {item.location.country}</span >
          <span > <strong>Postcode: </strong> {item.location.postcode} </span >
          <span> <Icon typeIcon="mail-1" />  <a href={`mailto:${item.email}`}>{item.email} </a> </span>
          <span > <Icon typeIcon="phone-1" /><a href={`tel:${item.phone}`}>{item.phone} </a></span >
          <span ><Icon typeIcon="mobile-1" /> <a href={`tel:${item.cell}`}>{item.cell} </a></span >
     </div>
     
        </div>
        </div>
        </div>
           <div className="bkg-modal"></div>
           </>
           )
        } 
     
      </div>
      <div className="footer">
        Development 	(CC BY) Paulo Correa
        </div> 
      </>
        ) 
 }    
}
}

export default App;
