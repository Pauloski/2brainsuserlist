import React, { Component } from 'react';
import withFirebaseAuth from 'react-with-firebase-auth'
import * as firebase from 'firebase/app';
import 'firebase/auth';
import firebaseConfig from './firebaseConfig';
import AppList from './AppList';
import Title from './components/atoms/Title';
import Button from './components/atoms/Button';
const firebaseApp = firebase.initializeApp(firebaseConfig);

class Login extends Component {
    render() {
      const {
        user,
        signOut,
        signInWithGoogle,
      } = this.props;
      
      return (
        <div>
            {
              user
                ? <div  >
                   <div className="user-display" > 
                   <div className="user-display__button">
                   <a href="#" onClick={signOut}>Sign out</a>
                   </div>
                   <p>Hello, {user.displayName}</p> 
                   
                   </div>
                    <AppList />
                  </div>
                : <div className="homeApp">
                  <div className="homeApp__info">
                 
                  <Title textComponent="#We Love C<>od3" fontSize={"heading1"} align ="center" weight="bold"/>
                  <p>Please sign in.</p>
                  <div className="homeApp__info-button"> 
                  <Button typeButton="default-p-s" textButton="Sign in with Google" actionButton={signInWithGoogle} />
                  </div>
                 
                  </div>
                   <video autoPlay muted loop id="myVideo">
          <source src={ require('./assets/image/video.mp4')} type="video/mp4" />
        </video>
                  </div>
            }
   {
          
                
            }
           
        </div>
      );
    }
  }

const firebaseAppAuth = firebaseApp.auth();

const providers = {
  googleProvider: new firebase.auth.GoogleAuthProvider(),
};

export default withFirebaseAuth({
  providers,
  firebaseAppAuth,
})(Login);